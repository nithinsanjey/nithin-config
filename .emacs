(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(backup-directory-alist (quote (("." . "~/.emacs.d/backups/"))))
 '(inhibit-startup-screen t)
 '(org-agenda-files nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Manages to install use-package - failed attempt
;;(eval-when-compile
;;  (add-to-list 'load-path "~/emacs-lisp")
;;  (require 'use-package))

;; Set the default mode of the scratch buffer to Org
(setq initial-major-mode 'org-mode)

(setq initial-scratch-message "\
# This buffer is for notes you don't want to save. You can use
# org-mode markup (and all Org's goodness) to organise the notes.
# If you want to create a file, visit that file with C-x C-f,
# then enter the text in that file's own buffer.
 
")

;; Adds markdown mode
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; Adds Github Flavoured markdown
(autoload 'gfm-mode "markdown-mode"
   "Major mode for editing GitHub Flavored Markdown files" t)
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))

;; If idle for more than 10 minutes, resolve the things by asking what to do
;; with the clock time
(setq org-clock-idle-time 10)

;; Logs time when item is done
(setq org-log-done 'time)

;; Sets M-x alternatives
(setq x-select-enable-clipboard t)
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-m" 'execute-extended-command)

;; Sets backspace alternatives
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)

;; Removes UGLY scroll bar, tool bar and menu bar
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))

;; (defalias 'qrr 'query-replace-regexp)

;; Keyboard macro for codeforces cpp template
(fset 'my-macro-cpp-template
   "#include<iostream>\C-musing namespace std;\C-m\C-mint main()\C-m{\C-m\C-mreturn 0;\C-m}\C-p\C-p\C-i\C-x\C-mend\C-ik\C-i")

;; keyboard macro for blogging
(fset 'my-macro-blog-template
   "---\C-mlayout: post\C-mtitile\C-?\C-?\C-?le: \"\"\C-mdescription: \"\"\C-mdate: 2018-\C-mte\C-?ags: []\C-mcomments: true\C-m---\C-p\C-p\C-p\C-p\C-p\C-f\C-f\C-f\C-f\C-f")
